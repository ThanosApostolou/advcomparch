# Install script for directory: /home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/inst/amd64-linux.gcc-serial")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "0")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake-2.6/Modules/Platform" TYPE FILE FILES
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Windows-ifort.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/SCO_SV.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/UNIX_SV.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-PGI-CXX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-Intel-Fortran.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/FreeBSD.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/xlf.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/UnixPaths.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Generic.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Darwin-xlc.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/GNU.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/MP-RAS.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Darwin-icpc.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/gas.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/RISCos.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/ULTRIX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/CYGWIN-g77.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Windows-g77.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-VisualAge-Fortran.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/WindowsPaths.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/OSF1.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/AIX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/DragonFly.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/gcc.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-como.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Generic-ADSP-ASM.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-Intel-CXX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/SunOS-SunPro-Fortran.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/HP-UX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Generic-SDCC-C.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/NetBSD.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Windows-cl.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-PGI-C.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-PGI-Fortran.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-GNU-Fortran.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/OpenBSD.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/BeOS.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/cl.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Windows.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/BSDOS.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/BlueGeneL.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-VisualAge-C.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/AIX-VisualAge-Fortran.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Darwin-icc.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/g77.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Generic-ADSP-C.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-Intel-C.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/IRIX64.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Generic-ADSP-CXX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Windows-bcc32.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/UnixWare.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Windows-df.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Windows-icl.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Tru64.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/SunOS.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Windows-g++.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/eCos.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/QNX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-SunPro-CXX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-ifort.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-icpc.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Windows-wcl386.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-SunPro-C.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/CYGWIN.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/IRIX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/kFreeBSD.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Linux-SunPro-Fortran.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Catamount.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/syllable.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/SINIX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Windows-gcc.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Xenix.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Darwin.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Generic-ADSP-Common.cmake"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake-2.6/Modules/Platform" TYPE FILE FILES "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Platform/Windows-cl.cmake.in")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

