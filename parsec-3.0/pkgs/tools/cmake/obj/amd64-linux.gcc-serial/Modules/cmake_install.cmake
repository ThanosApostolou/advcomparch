# Install script for directory: /home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/inst/amd64-linux.gcc-serial")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "0")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake-2.6/Modules" TYPE FILE FILES
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeVS71FindMake.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeVS7FindMake.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindPerl.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindProducer.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CTestTargets.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindLua51.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindBLAS.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindEXPAT.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindKDE3.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeTestJavaCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindGIF.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindGnuplot.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindSubversion.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeCInformation.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindWish.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindMotif.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindJPEG.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeExportBuildSettings.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPackDeb.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckCCompilerFlag.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FLTKCompatibility.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindGDAL.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgGA.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindPike.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindFreetype.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindDoxygen.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeBackwardCompatibilityC.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindSWIG.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgUtil.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgTerrain.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindUnixCommands.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakePrintSystemInformation.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeFindFrameworks.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindBZip2.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/UseVTKBuildSettings40.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMake.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindMPI.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeDetermineSystem.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindZLIB.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeDetermineJavaCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeFindWMake.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindTCL.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckIncludeFiles.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/UsewxWidgets.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckSymbolExists.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindJasper.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeMinGWFindMake.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgDB.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgSim.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/UsePkgConfig.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindCups.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeVS8FindMake.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindFLTK.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeDetermineFortranCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/TestBigEndian.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindSelfPackers.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeRCInformation.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeImportBuildSettings.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeVS6FindMake.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckStructHasMember.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindPackageMessage.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeCommonLanguageInclude.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckTypeSize.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/TestForSSTREAM.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/UseVTKConfig40.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindGLUT.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindSDL_net.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeTestFortranCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindwxWidgets.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindPerlLibs.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckCSourceCompiles.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Documentation.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeDetermineASM-ATTCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeDependentOption.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindQuickTime.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/KDE3Macros.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindQt.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindKDE4.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindPkgConfig.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindSDL_mixer.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeVS7BackwardCompatibility.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FeatureSummary.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeVS6BackwardCompatibility.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindTIFF.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckFortranFunctionExists.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindX11.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/MacroAddFileDependencies.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/InstallRequiredSystemLibraries.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeBorlandFindMake.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindASPELL.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckSizeOf.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindPythonLibs.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CTest.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindITK.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPackRPM.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindOpenThreads.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgIntrospection.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindImageMagick.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeFindBinUtils.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckCXXSourceRuns.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindThreads.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeDetermineASMCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindSDL_image.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindRuby.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/GetPrerequisites.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeCXXInformation.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/VTKCompatibility.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeTestCCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Dart.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindPythonInterp.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeDetermineCompilerId.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/TestForANSIForScope.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindJNI.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckCXXCompilerFlag.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeMSYSFindMake.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindLAPACK.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckCXXSourceCompiles.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindCABLE.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeDetermineCXXCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindDart.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/UseEcos.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeTestRCCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindOpenAL.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindHSPELL.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindMatlab.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindBoost.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Findosg.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeForceCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeTestASMCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindwxWindows.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/ecos_clean.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindQt4.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/ITKCompatibility.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindPHP4.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindSDL.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckCSourceRuns.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeASM-ATTInformation.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckIncludeFile.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgShadow.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeGenericSystem.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindJava.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindTclStub.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindWget.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/UseSWIG.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindSDL_ttf.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/TestForSTDNamespace.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindVTK.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/kde3uic.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindTclsh.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgViewer.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgFX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindPNG.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPack.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindGettext.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindSDL_sound.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindFLTK2.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgText.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindHTMLHelp.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindMPEG2.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeVS9FindMake.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindCygwin.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgParticle.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindOpenGL.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindLibXml2.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindPhysFS.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindCURL.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindGLU.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeFortranInformation.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/SystemInformation.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgManipulator.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckVariableExists.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindLibXslt.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeDetermineCCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/Use_wxWindows.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindMFC.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeDetermineCompilerABI.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeUnixFindMake.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindPackageHandleStandardArgs.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/AddFileDependencies.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindQt3.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeNMakeFindMake.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindosgProducer.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeFindXCode.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindCVS.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindLATEX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeJavaInformation.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindXMLRPC.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/TestCXXAcceptsFlag.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindLua50.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindOpenSSL.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeDetermineRCCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindMPEG.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeTestCXXCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindAVIFile.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindDCMTK.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeSystemSpecificInformation.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindGCCXML.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindCurses.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckIncludeFileCXX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/UseVTK40.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/TestForANSIStreamHeaders.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckFunctionExists.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPackZIP.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeASMInformation.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/FindGTK.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckLibraryExists.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeBackwardCompatibilityCXX.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeTestASM-ATTCompiler.cmake"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/UseQt4.cmake"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake-2.6/Modules" TYPE FILE FILES "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeCXXCompilerABI.cpp")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake-2.6/Modules" TYPE FILE FILES
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/TestForSSTREAM.cxx"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeTestForFreeVC.cxx"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/TestForANSIStreamHeaders.cxx"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/TestForSTDNamespace.cxx"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/DummyCXXFile.cxx"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/TestForAnsiForScope.cxx"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake-2.6/Modules" TYPE FILE FILES
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPack.DS_Store.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckLibraryExists.lists.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeJavaCompiler.cmake.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/kde3init_dummy.cpp.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeBuildSettings.cmake.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeASMCompiler.cmake.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckTypeSizeC.c.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeCCompilerId.c.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckIncludeFile.cxx.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakePlatformId.h.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeFortranCompiler.cmake.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeCCompiler.cmake.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckIncludeFile.c.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/NSIS.template.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPack.Info.plist.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/DartConfiguration.tcl.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeConfigurableFile.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPack.STGZ_Header.sh.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeRCCompiler.cmake.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/NSIS.InstallOptions.ini.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPack.background.png.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPack.Description.plist.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/TestEndianess.c.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/MacOSXBundleInfo.plist.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPack.distribution.dist.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPack.RuntimeScript.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeCXXCompilerId.cpp.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPack.OSXX11.Info.plist.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeFortranCompilerId.F90.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeCXXCompiler.cmake.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/SystemInformation.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPack.OSXScriptLauncher.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CPack.VolumeIcon.icns.in"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeSystem.cmake.in"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake-2.6/Modules" TYPE FILE FILES
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeTestNMakeCLVersion.c"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeCCompilerABI.c"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckFunctionExists.c"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckForPthreads.c"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeTestGNU.c"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CheckVariableExists.c"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake-2.6/Modules" TYPE FILE FILES "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/CMakeCompilerABI.h")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake-2.6/Modules" TYPE FILE FILES
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake-2.6/Modules" TYPE FILE FILES "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Modules/readme.txt")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Modules/Platform/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

