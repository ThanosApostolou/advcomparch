# Install script for directory: /home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/inst/amd64-linux.gcc-serial")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "0")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Source/kwsys/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Utilities/cmzlib/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Utilities/cmcurl/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Utilities/cmtar/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Utilities/cmcompress/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Utilities/cmexpat/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Utilities/cmxmlrpc/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Source/CursesDialog/form/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Source/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Modules/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Templates/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Utilities/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Tests/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
