# Install script for directory: /home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Utilities

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/inst/amd64-linux.gcc-serial")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "0")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/man/man1" TYPE FILE FILES "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/ctest.1")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/doc/cmake-2.6" TYPE FILE FILES
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/ctest.txt"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/ctest.html"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/ctest.docbook"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/man/man1" TYPE FILE FILES "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cpack.1")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/doc/cmake-2.6" TYPE FILE FILES
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cpack.txt"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cpack.html"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cpack.docbook"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/man/man1" TYPE FILE FILES "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/ccmake.1")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/doc/cmake-2.6" TYPE FILE FILES
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/ccmake.txt"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/ccmake.html"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/ccmake.docbook"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/man/man1" TYPE FILE FILES
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake.1"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmakecommands.1"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmakecompat.1"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmakeprops.1"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmakepolicies.1"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmakevars.1"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmakemodules.1"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/doc/cmake-2.6" TYPE FILE FILES
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake.txt"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake.html"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake.docbook"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake-policies.txt"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake-policies.html"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake-properties.txt"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake-properties.html"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake-variables.txt"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake-variables.html"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake-modules.txt"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake-modules.html"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake-commands.txt"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake-commands.html"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake-compatcommands.txt"
    "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Docs/cmake-compatcommands.html"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" MATCHES "^(Unspecified)$")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Utilities/Doxygen/cmake_install.cmake")
  INCLUDE("/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Utilities/KWStyle/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

