/*=========================================================================

  Program:   KWSys - Kitware System Library
  Module:    $RCSfile: testSystemTools.h.in,v $

  Copyright (c) Kitware, Inc., Insight Consortium.  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef cmsys_testSystemtools_h
#define cmsys_testSystemtools_h

#define EXECUTABLE_OUTPUT_PATH "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/obj/amd64-linux.gcc-serial/Source/kwsys"

#define TEST_SYSTEMTOOLS_BIN_FILE "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Source/kwsys/testSystemTools.bin"
#define TEST_SYSTEMTOOLS_SRC_FILE "/home/thanos/GIT/advcomparch/parsec-3.0/pkgs/tools/cmake/src/Source/kwsys/testSystemTools.cxx"

#endif
