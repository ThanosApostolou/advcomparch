# advcomparch

## example pin
```
cd pin-3.13-98189-g60a6ef199-gcc-linux/source/tools/ManualExamples
make obj-intel64/inscount0.so
cd ../../..
./pin -t ./source/tools/ManualExamples/obj-intel64/inscount0.so \
    -o ls.inscount0.output -- /bin/ls -aF
cat ls.inscount0.output
```

## build parsec benchmarks
```
cd parsec-3.0
./bin/parsecmgmt -a build -c gcc-serial -p blackscholes bodytrack canneal facesim ferret fluidanimate freqmine raytrace streamcluster swaptions
```

## pintool changes
makefile:
```
PIN_ROOT ?= /home/thanos/GIT/advcomparch/pin-3.13-98189-g60a6ef199-gcc-linux
```

cache.h:
```
UINT32 l2HitLatency = 15
UINT32 l2MissLatency = 250
```

tlb.h:
```
UINT32 MissLatency = 100
```

### build
```
cd pintool
make clean
make

export PIN_HOME="$HOME/GIT/advcomparch/pinplay-dcfg-3.11-pin-3.11-97998-g7ecce2dac-gcc-linux"
cd sniper-7.3
CC=gcc-7 CXX=g++-7 make
./run-sniper -g --perf_model/core/interval_timer/dispatch_width=8 -g --perf_model/core/interval_timer/window_size=256
```

### Run Sniper
```
export PIN_HOME="$HOME/GIT/advcomparch/pinplay-dcfg-3.11-pin-3.11-97998-g7ecce2dac-gcc-linux"
export PINBALLS_HOME="$HOME/GIT/advcomparch/sniper-7.3/cpu2006_pinballs"
cd sniper-7.3
mkdir -p outputs/gcc
./run-sniper -c gainestown -d outputs/gcc --pinballs=$PINBALLS_HOME/gcc/pinball_short.pp/pinball_t0r1_warmup3000_prolog0_region1000000002_epilog0_001_1-00000.0.address

/home/thanos/GIT/advcomparch/sniper-7.3/cpu2006_pinballs/gcc/pinball_short.pp/pinball_t0r1_warmup3000_prolog0_region1000000002_epilog0_001_1-00000.0.address
```