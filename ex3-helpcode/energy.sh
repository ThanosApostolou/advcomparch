#!/bin/bash

export ADVCOMPARCH_HOME="$HOME/GIT/advcomparch"
export PIN_HOME="$ADVCOMPARCH_HOME/pinplay-dcfg-3.11-pin-3.11-97998-g7ecce2dac-gcc-linux"
export SNIPER_EXE="$ADVCOMPARCH_HOME/sniper-7.3/run-sniper"
export SPEC_PINBALLS_DIR="$ADVCOMPARCH_HOME/sniper-7.3/cpu2006_pinballs"

cd outputs
for bench in *
do
    cd "$bench"
    for dir in *
    do
        if [ -d $dir ]; then
            cd $dir
            ../../../../sniper-7.3/tools/advcomparch_mcpat.py -d . -t total -o power > power.total.out
            cd ..
        fi
    done
    cd ..
done