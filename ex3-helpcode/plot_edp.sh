#!/bin/bash

cd power_diagrams
for dir in ../outputs/*
do
    echo ${dir}
    ../plot_edp.py ${dir}/*
done