#!/bin/bash

cd ipc_diagrams
for dir in ../outputs/*
do
    ../plot_ipc.py ${dir}/*
done