#!/bin/bash

cd power_diagrams
for dir in ../outputs/*
do
    echo ${dir}
    ../plot_power.py ${dir}/*
done