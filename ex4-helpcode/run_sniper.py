#!/bin/env python3

import os

HOME = os.environ["HOME"]
ADVCOMPARCH_HOME = f"{HOME}/GIT/advcomparch"
PIN_HOME = f"{ADVCOMPARCH_HOME}/pinplay-dcfg-3.11-pin-3.11-97998-g7ecce2dac-gcc-linux"
EX4_HELPCODE = f"{ADVCOMPARCH_HOME}/ex4-helpcode"
OUTPUT_DIR = f"{EX4_HELPCODE}/outputs/sniper"

os.environ["PIN_HOME"] = PIN_HOME

sniper_exe = f"{ADVCOMPARCH_HOME}/sniper-7.3/run-sniper -c {EX4_HELPCODE}/ask4.cfg"
mcpat_exe = f"{ADVCOMPARCH_HOME}/sniper-7.3/tools/advcomparch_mcpat.py"

lock_names = ["TAS_CAS", "TAS_TS", "TTAS_CAS", "TTAS_TS", "MUTEX"]
nthreads = [1, 2, 4, 8, 16]
grain_sizes = [1, 10, 100]
iterations = 1000

for grain in grain_sizes:
    for lock_name in lock_names:
        lock_exe = f"{EX4_HELPCODE}/bin/locks_SNIPER-{lock_name}"
        for n in nthreads:
            result_dir = f"{OUTPUT_DIR}/{grain}/{lock_name}-{n}"
            os.makedirs(result_dir, exist_ok = True)

            sniper_command = f"{sniper_exe} -d {result_dir} -n {n} --roi \
            -- {lock_exe} {n} {iterations} {grain}"

            mcpat_command = f"{mcpat_exe} -d {result_dir} -t total -o {result_dir}/power > {result_dir}/power.total.out"

            print(sniper_command)
            os.system(sniper_command)
            print(mcpat_command)
            os.system(mcpat_command)