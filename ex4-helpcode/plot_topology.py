#!/usr/bin/env python3

import sys, os
import itertools, operator
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

def get_params_from_sim_file(sim_file):
	cycles = 0
	runtime = 0
	fp = open(sim_file, "r")
	line = fp.readline()
	while line:
		if "Cycles" in line:
			cycles = float(line.split()[2])
		if "Time" in line:
			runtime = line.split()[3]
			runtime = float(runtime)
			runtime = runtime * 10**-9
			print("time " + str(runtime))
		line = fp.readline()
	fp.close()
	return (cycles, runtime)

def get_energy_from_power_file(power_file):
	energy = 0
	fp = open(power_file, "r")
	line = fp.readline()
	while line:
		if "total" in line:
			line = line[:-1]
			energy = line.split("total")[1]
			energy = energy.split("W")[1]
			if " mJ" in energy:
				energy = float(energy.split(" mJ")[0])
				energy = energy * 10**-3
			elif " kJ" in energy:
				energy = float(energy.split(" kJ")[0])
				energy = energy * 10**3
			else :
				energy = float(energy.split(" J")[0])
			print(f"energy {energy}")
		line = fp.readline()

	fp.close()
	return energy

def tuples_by_mechanism(tuples):
	ret = []
	tuples_sorted = sorted(tuples, key=operator.itemgetter(0))
	for key,group in itertools.groupby(tuples_sorted,operator.itemgetter(0)):
		ret.append((key, list(zip(*map(lambda x: x[1:], list(group))))))
	return ret

HOME = os.environ["HOME"]
ADVCOMPARCH_HOME = f"{HOME}/GIT/advcomparch"
PIN_HOME = f"{ADVCOMPARCH_HOME}/pinplay-dcfg-3.11-pin-3.11-97998-g7ecce2dac-gcc-linux"
EX4_HELPCODE = f"{ADVCOMPARCH_HOME}/ex4-helpcode"
OUTPUT_DIR = f"{EX4_HELPCODE}/outputs/topology"
PLOT_DIR = f"{EX4_HELPCODE}/plots/topology"

os.makedirs(PLOT_DIR, exist_ok = True)

topologies = ["share-all", "share-L3", "share-nothing"]
grain = 1

cycles_results_tuples = []
runtime_results_tuples = []
edp_results_tuples = []
for mechanism_n in sorted(os.listdir(OUTPUT_DIR)):
	mechanism_n_path = OUTPUT_DIR + "/" + mechanism_n
	sim_file = mechanism_n_path + "/sim.out"
	power_file = mechanism_n_path + "/power.total.out"

	mechanism = mechanism_n.split("-")[0]
	topology = mechanism_n.split("-")[1]
	(cycles, runtime) = get_params_from_sim_file(sim_file)
	energy = get_energy_from_power_file(power_file)
	edp = energy * runtime
	print(sim_file,mechanism, topology, cycles, edp)
	cycles_results_tuples.append((mechanism, topology, cycles))
	runtime_results_tuples.append((mechanism, topology, runtime))
	edp_results_tuples.append((mechanism, topology, edp))

# generate cycles plot
markers = ['.', 'o', 'v', '*', 'D']
fig = plt.figure()
plt.grid(True)
ax = plt.subplot(111)
ax.set_xlabel("$Topology$")
ax.set_ylabel("$Cycles$")

i = 0
tuples = tuples_by_mechanism(cycles_results_tuples)
for tuple in tuples:
	mechanism = tuple[0]
	ws_axis = tuple[1][0]
	edp_axis = tuple[1][1]
	x_ticks = np.arange(0, len(topologies))
	x_labels = map(str, topologies)
	ax.xaxis.set_ticks(x_ticks)
	ax.xaxis.set_ticklabels(x_labels)

	print(x_ticks)
	print(edp_axis)
	ax.plot(x_ticks, edp_axis, label=str(mechanism), marker=markers[i%len(markers)])
	i = i + 1

lgd = ax.legend(ncol=len(tuples), bbox_to_anchor=(0.9, -0.1), prop={'size':8})
plt.savefig(f"{PLOT_DIR}/Topology.cycles.png", bbox_extra_artists=(lgd,), bbox_inches='tight')

# generate runtime plot
markers = ['.', 'o', 'v', '*', 'D']
fig = plt.figure()
plt.grid(True)
ax = plt.subplot(111)
ax.set_xlabel("$Topology$")
ax.set_ylabel("$Runtime$")

i = 0
tuples = tuples_by_mechanism(runtime_results_tuples)
for tuple in tuples:
	mechanism = tuple[0]
	ws_axis = tuple[1][0]
	edp_axis = tuple[1][1]
	x_ticks = np.arange(0, len(topologies))
	x_labels = map(str, topologies)
	ax.xaxis.set_ticks(x_ticks)
	ax.xaxis.set_ticklabels(x_labels)

	print(x_ticks)
	print(edp_axis)
	ax.plot(x_ticks, edp_axis, label=str(mechanism), marker=markers[i%len(markers)])
	i = i + 1

lgd = ax.legend(ncol=len(tuples), bbox_to_anchor=(0.9, -0.1), prop={'size':8})
plt.savefig(f"{PLOT_DIR}/Topology.runtime.png", bbox_extra_artists=(lgd,), bbox_inches='tight')

# generate edp plot
markers = ['.', 'o', 'v', '*', 'D']
fig = plt.figure()
plt.grid(True)
ax = plt.subplot(111)
ax.set_xlabel("$Topology$")
ax.set_ylabel("$EDP$")

i = 0
tuples = tuples_by_mechanism(edp_results_tuples)
for tuple in tuples:
	mechanism = tuple[0]
	ws_axis = tuple[1][0]
	edp_axis = tuple[1][1]
	x_ticks = np.arange(0, len(topologies))
	x_labels = map(str, topologies)
	ax.xaxis.set_ticks(x_ticks)
	ax.xaxis.set_ticklabels(x_labels)

	print(x_ticks)
	print(edp_axis)
	ax.plot(x_ticks, edp_axis, label=str(mechanism), marker=markers[i%len(markers)])
	i = i + 1

lgd = ax.legend(ncol=len(tuples), bbox_to_anchor=(0.9, -0.1), prop={'size':8})
plt.savefig(f"{PLOT_DIR}/Topology.edp.png", bbox_extra_artists=(lgd,), bbox_inches='tight')
