#!/bin/env python3

import os

HOME = os.environ["HOME"]
ADVCOMPARCH_HOME = f"{HOME}/GIT/advcomparch"
PIN_HOME = f"{ADVCOMPARCH_HOME}/pinplay-dcfg-3.11-pin-3.11-97998-g7ecce2dac-gcc-linux"
EX4_HELPCODE = f"{ADVCOMPARCH_HOME}/ex4-helpcode"
OUTPUT_DIR = f"{EX4_HELPCODE}/outputs/topology"

os.environ["PIN_HOME"] = PIN_HOME

sniper_exe = f"{ADVCOMPARCH_HOME}/sniper-7.3/run-sniper -c {EX4_HELPCODE}/ask4.cfg"
mcpat_exe = f"{ADVCOMPARCH_HOME}/sniper-7.3/tools/advcomparch_mcpat.py"

lock_names = ["TAS_CAS", "TAS_TS", "TTAS_CAS", "TTAS_TS", "MUTEX"]
n = 4
grain = 1
iterations = 1000

for lock_name in lock_names:
    lock_exe = f"{EX4_HELPCODE}/bin/locks_SNIPER-{lock_name}"

    # share-all
    result_dir = f"{OUTPUT_DIR}/{lock_name}-share_all"
    os.makedirs(result_dir, exist_ok = True)
    sniper_command = f"{sniper_exe} -d {result_dir} -n {n} --roi \
    -g --perf_model/l1_icache/shared_cores=1 \
    -g --perf_model/l1_dcache/shared_cores=1 \
    -g --perf_model/l2_cache/shared_cores=4 \
    -g --perf_model/l3_cache/shared_cores=4 \
    -- {lock_exe} {n} {iterations} {grain}"
    mcpat_command = f"{mcpat_exe} -d {result_dir} -t total -o {result_dir}/power > {result_dir}/power.total.out"
    print(sniper_command)
    os.system(sniper_command)
    print(mcpat_command)
    os.system(mcpat_command)

    # share-L3
    result_dir = f"{OUTPUT_DIR}/{lock_name}-share_L3"
    os.makedirs(result_dir, exist_ok = True)
    sniper_command = f"{sniper_exe} -d {result_dir} -n {n} --roi \
    -g --perf_model/l1_icache/shared_cores=1 \
    -g --perf_model/l1_dcache/shared_cores=1 \
    -g --perf_model/l2_cache/shared_cores=1 \
    -g --perf_model/l3_cache/shared_cores=4 \
    -- {lock_exe} {n} {iterations} {grain}"
    mcpat_command = f"{mcpat_exe} -d {result_dir} -t total -o {result_dir}/power > {result_dir}/power.total.out"
    print(sniper_command)
    os.system(sniper_command)
    print(mcpat_command)
    os.system(mcpat_command)

    # share-nothing
    result_dir = f"{OUTPUT_DIR}/{lock_name}-share_nothing"
    os.makedirs(result_dir, exist_ok = True)
    sniper_command = f"{sniper_exe} -d {result_dir} -n {n} --roi \
    -g --perf_model/l1_icache/shared_cores=1 \
    -g --perf_model/l1_dcache/shared_cores=1 \
    -g --perf_model/l2_cache/shared_cores=1 \
    -g --perf_model/l3_cache/shared_cores=1 \
    -- {lock_exe} {n} {iterations} {grain}"
    mcpat_command = f"{mcpat_exe} -d {result_dir} -t total -o {result_dir}/power > {result_dir}/power.total.out"
    print(sniper_command)
    os.system(sniper_command)
    print(mcpat_command)
    os.system(mcpat_command)