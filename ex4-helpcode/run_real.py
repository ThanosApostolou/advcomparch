#!/bin/env python3

import os

HOME = os.environ["HOME"]
ADVCOMPARCH_HOME = f"{HOME}/GIT/advcomparch"
PIN_HOME = f"{ADVCOMPARCH_HOME}/pinplay-dcfg-3.11-pin-3.11-97998-g7ecce2dac-gcc-linux"
EX4_HELPCODE = f"{ADVCOMPARCH_HOME}/ex4-helpcode"
OUTPUT_DIR = f"{EX4_HELPCODE}/outputs/real"

os.environ["PIN_HOME"] = PIN_HOME

lock_names = ["TAS_CAS", "TAS_TS", "TTAS_CAS", "TTAS_TS", "MUTEX"]
nthreads = [1, 2, 4, 8]
grain_sizes = [1, 10, 100]
iterations = 4000000

for grain in grain_sizes:
    for lock_name in lock_names:
        lock_exe = f"{EX4_HELPCODE}/bin/locks_REAL-{lock_name}"
        for n in nthreads:
            result_dir = f"{OUTPUT_DIR}/{grain}/{lock_name}-{n}"
            os.makedirs(result_dir, exist_ok = True)

            command = f"{lock_exe} {n} {iterations} {grain} > {result_dir}/sim.out"

            print(command)
            os.system(command)