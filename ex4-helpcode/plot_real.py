#!/usr/bin/env python3

import sys, os
import itertools, operator
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

def get_params_from_sim_file(sim_file):
	runtime = 0
	fp = open(sim_file, "r")
	line = fp.readline()
	while line:
		if "time" in line:
			runtime = line.split("time:")[1]
			runtime = runtime.split(" seconds")[0]
			runtime = float(runtime)
			print("time " + str(runtime))
		line = fp.readline()
	fp.close()
	return runtime

def tuples_by_mechanism(tuples):
	ret = []
	tuples_sorted = sorted(tuples, key=operator.itemgetter(0))
	for key,group in itertools.groupby(tuples_sorted,operator.itemgetter(0)):
		ret.append((key, list(zip(*map(lambda x: x[1:], list(group))))))
	return ret

HOME = os.environ["HOME"]
ADVCOMPARCH_HOME = f"{HOME}/GIT/advcomparch"
PIN_HOME = f"{ADVCOMPARCH_HOME}/pinplay-dcfg-3.11-pin-3.11-97998-g7ecce2dac-gcc-linux"
EX4_HELPCODE = f"{ADVCOMPARCH_HOME}/ex4-helpcode"
OUTPUT_DIR = f"{EX4_HELPCODE}/outputs/real"
PLOT_DIR = f"{EX4_HELPCODE}/plots/real"
nthreads = [1, 2, 4, 8]
grain_sizes = [1, 10, 100]

os.makedirs(PLOT_DIR, exist_ok = True)

for grain in grain_sizes:
	grain_path = f"{OUTPUT_DIR}/{grain}"
	results_tuples = []
	for mechanism_n in sorted(os.listdir(grain_path)):
		mechanism_n_path = grain_path + "/" + mechanism_n
		sim_file = mechanism_n_path + "/sim.out"

		mechanism = mechanism_n.split("-")[0]
		n = mechanism_n.split("-")[1]
		runtime = get_params_from_sim_file(sim_file)
		print(sim_file,mechanism, n, runtime)
		results_tuples.append((mechanism, n, runtime))

	# generate runtime plot
	markers = ['.', 'o', 'v', '*', 'D']
	fig = plt.figure()
	plt.grid(True)
	ax = plt.subplot(111)
	ax.set_xlabel("$NThreads$")
	ax.set_ylabel("$Runtime$")

	i = 0
	tuples = tuples_by_mechanism(results_tuples)
	for tuple in tuples:
		mechanism = tuple[0]
		ws_axis = tuple[1][0]
		edp_axis = tuple[1][1]
		x_ticks = np.arange(0, len(nthreads))
		x_labels = map(str, nthreads)
		ax.xaxis.set_ticks(x_ticks)
		ax.xaxis.set_ticklabels(x_labels)

		print(x_ticks)
		print(edp_axis)
		ax.plot(x_ticks, edp_axis, label=str(mechanism), marker=markers[i%len(markers)])
		i = i + 1

	lgd = ax.legend(ncol=len(tuples), bbox_to_anchor=(0.9, -0.1), prop={'size':8})
	plt.savefig(f"{PLOT_DIR}/Grain{grain}.runtime.png", bbox_extra_artists=(lgd,), bbox_inches='tight')